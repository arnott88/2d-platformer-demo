﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Prime31;

public class PlayerController : MonoBehaviour
{

    public enum GroundType
    {
        None,
        LevelGeometry,
        OneWayPlatfrom,
        MovingPlatform,
        CollapsablePlatform
    }


    //player control parameters
    public float walkSpeed = 10.0f;
    public float jumpSpeed = 25.0f;
    public float gravity = 40.0f;
    public float doubleJumpSpeed = 12.0f;
    public float wallJumpXAmount = 1f;
    public float wallJumpYAmount = 1f;
    public float wallRunAmount = 4f;
    public float slopeSlideSpeed = 12f;
    public float glideAmount = 2f;
    public float glideTimer = 2f;
    public float creepSpeed = 5.0f;
    public float powerJumpSpeed = 10.0f;
    public float stompSpeed = 6.0f;
    public int tapCount = 0;
    public float dashSpeed = 20.0f;

    

    //player ability toggles
    public bool canDoubleJump = true;
    public bool canWallJump = true;
    public bool canJumpAfterWallJump = true;
    public bool canWallRun = true;
    public bool canRunAfterWallJump = true;
    public bool canGlide = true;
    public bool canPowerJump = true;
    public bool canStomp = true;
    public bool canDash = true;

    //player state variables
    public bool isGrounded;
    public bool isJumping;
    public bool isFacingRight;
    public bool doubleJumped;
    public bool wallJumped;
    public bool isWallRunning;
    public bool isSlopeSliding;
    public bool isGliding;
    public bool isDucking;
    public bool isCreeping;
    public bool isPowerJumping;
    public bool isStomping;
    public bool hasDoubleTapped;
    public bool isDashing;
    public bool isBackDashing;
    public bool isPunching;


    public LayerMask layerMask;

    //private variables
    private CharacterController2D.CharacterCollisionState2D _flags;
    private Vector3 _moveDirection = Vector3.zero;
    private CharacterController2D _characterController;
    private bool _lastJumpWasLeft;
    private float _slopeAngle;
    private Vector3 _slopeGradient = Vector3.zero;
    private bool _startGlide;
    private float _currentGlideTimer;
    private BoxCollider2D _boxCollider;
    private Vector2 _originalBoxCollideSize;
    private Vector3 _frontTopCorner;
    private Vector3 _backTopCorner;
    private Vector3 _tripHeight;
    private bool _tripLimit;
    private Animator _animator;
    private bool _ableToWallRun;
    private float _startDash;
    private GroundType _groundType;
    private GameObject _tempOneWayPlatform;
    private GameObject _tempMovingPlatform;
    public int _doubleTapCount;
    private Vector3 _movingPlatformVelocity = Vector3.zero;


    


    // Start is called before the first frame update
    void Start()
    {
        _characterController = GetComponent<CharacterController2D>();
        _currentGlideTimer = glideTimer;
        _boxCollider = GetComponent<BoxCollider2D>();
        _originalBoxCollideSize = _boxCollider.size;
        _animator = GetComponentInChildren<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

        //sets the player movement on X based on user input
        if (wallJumped == false && _tripLimit == false) 
        {
            _moveDirection.x = Input.GetAxis("Horizontal");
            if(isCreeping)
            {
                _moveDirection.x *= creepSpeed;
            }
            else if(isDashing)
            {
                _moveDirection.x *= dashSpeed;               
            }  
            else
            {
                _moveDirection.x *= walkSpeed;
            }
        }


        


        #region Player is grounded
        if (isGrounded) //player is grounded
        {
            _moveDirection.y = 0;
            isJumping = false;
            doubleJumped = false;
            isStomping = false;
            hasDoubleTapped = false;
            _currentGlideTimer = glideTimer;
            tapCount = 0;
            isDashing = false;

            //flip character
            if (_moveDirection.x < 0)
            {
                transform.eulerAngles = new Vector3(0, 180, 0);
                isFacingRight = false;
            } 
            else if(_moveDirection.x > 0)
            {
                transform.eulerAngles = new Vector3(0, 0, 0);
                isFacingRight = true;
            }

            if(isSlopeSliding)
            {
                _moveDirection = new Vector3(_slopeGradient.x * slopeSlideSpeed, -_slopeGradient.y * slopeSlideSpeed, 0f);
            }

            if (Input.GetButtonDown("Jump"))
            {
                if(canPowerJump && isDucking)
                {
                    _moveDirection.y = jumpSpeed + powerJumpSpeed;
                    StartCoroutine(PowerJumpWaiter());
                }
                else
                {
                    _moveDirection.y = jumpSpeed;
                    isJumping = true;
                }                
                _ableToWallRun = true;
            }
            //dashing
            if(canDash == true && Input.GetButtonDown("Dash"))
            {
                StartCoroutine(DashTimer());
            }
            //punch
            if(Input.GetButtonDown("Punch") && !isDucking && !isCreeping)
            {
                isPunching = true;
                StartCoroutine(PunchTime());   
            }
            //fall through one way platforms
            if(_groundType.Equals(GroundType.OneWayPlatfrom))
            {
                if (Input.GetButtonDown("Vertical"))
                {
                    _doubleTapCount++;
                    if (_doubleTapCount >= 2)
                    {
                        StartCoroutine(DisableOneWayPlatform());
                    }
                    StartCoroutine(DoubleTapTimer());
                }
            }
        }
        #endregion

        #region Player is in air
        else //player is in the air
        {
            if(Input.GetButtonUp("Jump"))
            {
                if(_moveDirection.y > 0)
                {
                    _moveDirection.y = _moveDirection.y * 0.5f;
                }
            }
            if(Input.GetButtonDown("Jump"))
            {
                if(canDoubleJump)
                {
                    if(!doubleJumped)
                    {
                        _moveDirection.y = doubleJumpSpeed;
                        doubleJumped = true;
                    }
                }
            }
        }
        #endregion

        #region Gravity calculations

        if (Input.GetButtonDown("Vertical"))
        {
            Debug.Log("inside jump and down");
            tapCount++;
        }
        if(tapCount == 2)
        {
            hasDoubleTapped = true;
        }
            
        //glide
        if (canGlide == true && Input.GetAxis("Glide") > 0 && _characterController.velocity.y < 0.2f)
        {
            if(_currentGlideTimer > 0)
            {
                isGliding = true;
                if (_startGlide)
                {
                    _moveDirection.y = 0;
                    _startGlide = false;
                }
                _moveDirection.y -= glideAmount * Time.deltaTime;
                _currentGlideTimer -= Time.deltaTime;
            }
            else
            {
                isGliding = false;
                _moveDirection.y -= gravity * Time.deltaTime;
            }
            
        }
        //stomp
        else if(canStomp && hasDoubleTapped && !isPowerJumping)
        {
            _moveDirection.y -= gravity * Time.deltaTime + stompSpeed;
            isStomping = true;
        }
        else
        {
            isGliding = false;
            _startGlide = true;
            _moveDirection.y -= gravity * Time.deltaTime;
        }

        #endregion

        #region moving platform adjustment

        if(_tempMovingPlatform && _groundType.Equals(GroundType.MovingPlatform))
        {
            _movingPlatformVelocity = _tempMovingPlatform.GetComponent<MovingPlatform>().difference;
        }

        if(!isGrounded && !isGliding)
        {
            if(_moveDirection.x > 0 && _movingPlatformVelocity.x > 0)
            {
                _moveDirection.x += _movingPlatformVelocity.x;
            }
            else if(_moveDirection.x < 0 && _movingPlatformVelocity.x < 0)
            {
                _moveDirection.x += _movingPlatformVelocity.x;
            }
        }

        #endregion



        //update the characterController2d
        _characterController.move(_moveDirection * Time.deltaTime);
        _flags = _characterController.collisionState;



        #region Ducking and creeping
        _frontTopCorner = new Vector3(transform.position.x + _boxCollider.size.x / 2, transform.position.y + _boxCollider.size.x / 2);
        _backTopCorner = new Vector3(transform.position.x - _boxCollider.size.x / 2, transform.position.y + _boxCollider.size.x / 2);

        RaycastHit2D hitFrontCeiling = Physics2D.Raycast(_frontTopCorner, Vector2.up, 2f, layerMask);
        RaycastHit2D hitBackCeiling = Physics2D.Raycast(_backTopCorner, Vector2.up, 2f, layerMask);


        if (Input.GetAxis("Vertical") < 0 && _moveDirection.x == 0 && isGrounded && !isPunching)
        {
            if(!isDucking && !isCreeping)
            {
                _boxCollider.size = new Vector2(_boxCollider.size.x, _originalBoxCollideSize.y / 2);
                transform.position = new Vector3(transform.position.x, transform.position.y - (_originalBoxCollideSize.y / 4), 0);
                _characterController.recalculateDistanceBetweenRays();
            }
            isDucking = true;
            isCreeping = false;
        }
        else if(Input.GetAxis("Vertical") < 0 && (_moveDirection.x < 0 || _moveDirection.x > 0) && isGrounded && !isPunching)
        {
            if(!isDucking && !isCreeping)
            {
                _boxCollider.size = new Vector2(_boxCollider.size.x, _originalBoxCollideSize.y / 2);
                transform.position = new Vector3(transform.position.x, transform.position.y - (_originalBoxCollideSize.y / 4), 0);
                _characterController.recalculateDistanceBetweenRays();
                
            }
            isDucking = false;
            isCreeping = true;
        }
        else
        {
            if(!hitFrontCeiling.collider &&  !hitBackCeiling.collider && (isDucking || isCreeping))
            {
                _boxCollider.size = new Vector2(_boxCollider.size.x, _originalBoxCollideSize.y);
                transform.position = new Vector3(transform.position.x, transform.position.y + (_originalBoxCollideSize.y / 4), 0);
                _characterController.recalculateDistanceBetweenRays();

                isCreeping = false;
                isDucking = false;
            }
            
        }

        #endregion

        if (_flags.above)
        {
            _moveDirection.y -= gravity * Time.deltaTime;
        }

        #region Wall running & jumping
        if (_flags.left || _flags.right)
        {
            if(canWallJump)
            {
                if(canWallRun)
                {
                    if(Input.GetAxis("Vertical") > 0 && _ableToWallRun == true && isGrounded == false)
                    {
                        _moveDirection.y = jumpSpeed / wallRunAmount;
                        StartCoroutine(WallRunWaiter());

                        if(_flags.left)
                        {
                            transform.eulerAngles = new Vector3(0, 180, 0);
                        }
                        else if(_flags.right)
                        {
                            transform.eulerAngles = new Vector3(0, 0, 0);
                        }
                    }
                }

                if(Input.GetButtonDown("Jump") && wallJumped == false && isGrounded == false)
                {
                    if(_moveDirection.x < 0)
                    {
                        _moveDirection.x = jumpSpeed * wallJumpXAmount;
                        _moveDirection.y = jumpSpeed * wallJumpYAmount;
                        transform.eulerAngles = new Vector3(0, 0, 0);
                        _lastJumpWasLeft = false;
                    }
                    else if(_moveDirection.x > 0)
                    {
                        _moveDirection.x = -jumpSpeed * wallJumpXAmount;
                        _moveDirection.y = jumpSpeed * wallJumpYAmount;
                        transform.eulerAngles = new Vector3(0, 180, 0);
                        _lastJumpWasLeft = true;
                    }

                    StartCoroutine(WallJumpWaiter());

                    if(canJumpAfterWallJump)
                    {
                        doubleJumped = false;
                    }
                    if(canRunAfterWallJump == false)
                    {
                        _ableToWallRun = false;
                    }
                }
            }
        }
        else
        {
            if(canRunAfterWallJump)
            {
                StopCoroutine(WallRunWaiter());
                _ableToWallRun = true;
                isWallRunning = false;
            }
        }

        #endregion

        isGrounded = _flags.below;
        if(isGrounded)
        {
            GetGroundType();
        }
        else if(!isGrounded && !_flags.wasGroundedLastFrame)
        {
            ClearGroundType();
        }

        
        UpdateAnimator();

    }

    #region IEnumerators

    IEnumerator WallJumpWaiter()
    {
        wallJumped = true;
        yield return new WaitForSeconds(0.5f);
        wallJumped = false;
    }
    IEnumerator WallRunWaiter()
    {
        isWallRunning = true;
        yield return new WaitForSeconds(0.5f);
        isWallRunning = false;
        if(wallJumped == false)
        {
            _ableToWallRun = false;

        }
    }
    IEnumerator PowerJumpWaiter()
    {
        isPowerJumping = true;
        yield return new WaitForSeconds(1f);
        isPowerJumping = false;
    }
    IEnumerator DashTimer()
    {
        isDashing = true;
        gravity = 0f;
        yield return new WaitForSeconds(.25f);
        gravity = 10f;
        yield return new WaitForSeconds(.25f);
        gravity = 40.0f;
        isDashing = false;
    }
    IEnumerator PunchTime()
    {
        yield return new WaitForSeconds(.01f);
        isPunching = false;
    }
    IEnumerator DoubleTapTimer()
    {
        yield return new WaitForSeconds(0.5f);
        _doubleTapCount = 0;
        
    }
    IEnumerator DisableOneWayPlatform()
    {
        if(_tempOneWayPlatform)
        {
            _tempOneWayPlatform.GetComponent<EdgeCollider2D>().enabled = false;
            yield return new WaitForSeconds(.7f);
            if(_tempOneWayPlatform)
            {
                _tempOneWayPlatform.GetComponent<EdgeCollider2D>().enabled = true;
                _tempOneWayPlatform = null;
            }
        }
    }
    

    #endregion

    private void GetGroundType()
    {
        //check ground angle for slope sliding
        RaycastHit2D hit = Physics2D.Raycast(transform.position, -Vector3.up, 2f, layerMask);
        if(hit)
        {
            _slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
            _slopeGradient = hit.normal;

            if(_slopeAngle > _characterController.slopeLimit)
            {
                isSlopeSliding = true;
            }
            else
            {
                isSlopeSliding = false;
            }

            string layerName = LayerMask.LayerToName(hit.transform.gameObject.layer);
            if(layerName == "OneWayPlatform")
            {
                _groundType = GroundType.OneWayPlatfrom;
                if(!_tempOneWayPlatform)
                {
                    _tempOneWayPlatform = hit.transform.gameObject;
                }
            }
            else if(layerName == "LevelGeometry")
            {
                _groundType = GroundType.LevelGeometry;
            }
            else if(layerName == "MovingPlatform")
            {
                _groundType = GroundType.MovingPlatform;
                if(!_tempMovingPlatform)
                {
                    _tempMovingPlatform = hit.transform.gameObject;
                    transform.SetParent(hit.transform);
                }
            }
            else if(layerName == "CollapsablePlatform")
            {
                _groundType = GroundType.CollapsablePlatform;
                StartCoroutine(hit.transform.gameObject.GetComponent<CollapsablePlatform>().CollapseTimer());
                transform.SetParent(hit.transform);
            }
        }
    }

    private void ClearGroundType()
    {
        _groundType = GroundType.None;
        if (_tempMovingPlatform)
        {
            _tempMovingPlatform = null;
        }
        transform.SetParent(null);
    }

    private void UpdateAnimator()
    {
        _animator.SetFloat("movementX", Mathf.Abs(_moveDirection.x / walkSpeed));
        _animator.SetFloat("movementY", _moveDirection.y);
        _animator.SetBool("isGrounded", isGrounded);
        _animator.SetBool("isJumping", isJumping);
        _animator.SetBool("doubleJumped", doubleJumped);
        _animator.SetBool("wallJumped", wallJumped);
        _animator.SetBool("isWallRunning", isWallRunning);
        _animator.SetBool("isGliding", isGliding);
        _animator.SetBool("isDucking", isDucking);
        _animator.SetBool("isCreeping", isCreeping);
        _animator.SetBool("isPowerJumping", isPowerJumping);
        _animator.SetBool("isStomping", isStomping);
        _animator.SetBool("isSlopeSliding", isSlopeSliding);
        _animator.SetBool("isDashing", isDashing);
        _animator.SetBool("isBackDashing", isBackDashing);
        _animator.SetBool("isPunching", isPunching);
    }

}
