﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollapsablePlatform : MonoBehaviour
{
   
    public void CollapsePlatform()
    {
        Rigidbody2D rigidBody2D = GetComponent<Rigidbody2D>();
        rigidBody2D.bodyType = RigidbodyType2D.Dynamic;
        rigidBody2D.mass = 3;
    }
  
    public IEnumerator CollapseTimer()
    {
        yield return new WaitForSeconds(1f);
        CollapsePlatform();
        yield return new WaitForSeconds(3f);
        GameObject.Destroy(gameObject);
    }

}
