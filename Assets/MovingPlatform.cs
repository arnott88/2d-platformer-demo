﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{

    private Vector3 _lastPosition;
    public Vector3 difference;

    // Start is called before the first frame update
    void Start()
    {
        _lastPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        difference = transform.position - _lastPosition;
        difference = difference / Time.deltaTime;
        _lastPosition = transform.position;
    }
}
